/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
//var app = {
//    // Application Constructor
//    initialize: function() {
//        this.bindEvents();
//    },
//    // Bind Event Listeners
//    //
//    // Bind any events that are required on startup. Common events are:
//    // 'load', 'deviceready', 'offline', and 'online'.
//    bindEvents: function() {
//        document.addEventListener('deviceready', this.onDeviceReady, false);
//    },
//    // deviceready Event Handler
//    //
//    // The scope of 'this' is the event. In order to call the 'receivedEvent'
//    // function, we must explicitly call 'app.receivedEvent(...);'
//    onDeviceReady: function() {
//        app.receivedEvent('deviceready');
//    },
//    // Update DOM on a Received Event
//    receivedEvent: function(id) {
//        var parentElement = document.getElementById(id);
//        var listeningElement = parentElement.querySelector('.listening');
//        var receivedElement = parentElement.querySelector('.received');
//
//        listeningElement.setAttribute('style', 'display:none;');
//        receivedElement.setAttribute('style', 'display:block;');
//
//        console.log('Received Event: ' + id);
//    }
//};
//
//app.initialize();
$(document).ready(function (){
   getCustomerId();
   getInventoryId();
   getOrdersId();
   getAskId();
   changeloadColor();
});
function changeloadColor()
{
//   for(var i=0;i<17;i++)
//   {
//       $(".fil"+i).css("fill","#fff");
       //$(".fil"+i).css("fill","#fff");
   
//        $(".fil0").css("fill","#159588");
//        $(".fil1").css("fill","#1EAAF1");
//        $(".fil2").css("fill","#1FBCD2");
//        $(".fil3").css("fill","#2C992D");
//        $(".fil4").css("fill","#4054B2");
//        $(".fil5").css("fill","#587BF8");
//        $(".fil6").css("fill","#663EB4");
//        $(".fil7").css("fill","#8CC152");
//        $(".fil8").css("fill","#9A2FAE");
//        $(".fil9").css("fill","#CDDA49");
//        $(".fil10").css("fill","#DA2128");
//        $(".fil11").css("fill","#E2202C");
//        $(".fil12").css("fill","#E62565");
//        $(".fil13").css("fill","#FC5830");
//        $(".fil14").css("fill","#FC9727");
//        $(".fil15").css("fill","#FDC02F");
//        $(".fil16").css("fill","#FEE94E");
//        $(".fil17").css("fill","#373435");
        
   // }
        
      //$(".fil0").animate({fill:"#159588"}, 4000);
        //$(".fil0").animate({fill:"#159588"},{duration:4000});
//        $(".fil1").animate({fill:"#1EAAF1"},4000);
//        $(".fil2").animate({fill:"#1FBCD2"},4000);
//        $(".fil3").animate({fill:"#2C992D"},4000);
//        $(".fil4").animate({fill:"#4054B2"},4000);
//        $(".fil5").animate({fill:"#587BF8"},4000);
//        $(".fil6").animate({fill:"#663EB4"},4000);
//        $(".fil7").animate({fill:"#8CC152"},4000);
//        $(".fil8").animate({fill:"#9A2FAE"},4000);
//        $(".fil9").animate({fill:"#CDDA49"},4000);
//        $(".fil10").animate({fill:"#DA2128"},4000);
//        $(".fil11").animate({fill:"#E2202C"},4000);
//        $(".fil12").animate({fill:"#E62565"},4000);
//        $(".fil13").animate({fill:"#FC5830"},4000);
//        $(".fil14").animate({fill:"#FC9727"},4000);
//        $(".fil15").animate({fill:"#FDC02F"},4000);
//        $(".fil16").animate({fill:"#FEE94E"},4000);
//        $(".fil17").animate({fill:"#373435"},4000);
        //$(".fil17").animate({fill:'#159588'});
}
$(function() {

   $(".input input").focus(function() {

      $(this).parent(".input").each(function() {
         $("label", this).css({
            "line-height": "18px",
            "font-size": "18px",
            "font-weight": "100",
            "top": "0px"
         })
         $(".spin", this).css({
            "width": "100%"
         })
      });
   }).blur(function() {
      $(".spin").css({
         "width": "0px"
      })
      if ($(this).val() == "") {
         $(this).parent(".input").each(function() {
            $("label", this).css({
               "line-height": "60px",
               "font-size": "24px",
               "font-weight": "300",
               "top": "10px"
            })
         });

      }
   });

   $(".button").click(function(e) {
      var pX = e.pageX,
         pY = e.pageY,
         oX = parseInt($(this).offset().left),
         oY = parseInt($(this).offset().top);

      $(this).append('<span class="click-efect x-' + oX + ' y-' + oY + '" style="margin-left:' + (pX - oX) + 'px;margin-top:' + (pY - oY) + 'px;"></span>')
      $('.x-' + oX + '.y-' + oY + '').animate({
         "width": "500px",
         "height": "500px",
         "top": "-250px",
         "left": "-250px",

      }, 600);
      $("button", this).addClass('active');
   })

   $(".alt-2").click(function() {
      if (!$(this).hasClass('material-button')) {
         $(".shape").css({
            "width": "100%",
            "height": "100%",
            "transform": "rotate(0deg)"
         })

         setTimeout(function() {
            $(".overbox").css({
               "overflow": "initial"
            })
         }, 600)

         $(this).animate({
            "width": "100px",
            "height": "100px"
         }, 500, function() {
            $(".box").removeClass("back");

            $(this).removeClass('active')
         });

         $(".overbox .title").fadeOut(300);
         $(".overbox .input").fadeOut(300);
         $(".overbox .button").fadeOut(300);

         $(".alt-2").addClass('material-buton');
      }

   })

   $(".material-button").click(function() {

      if ($(this).hasClass('material-button')) {
         setTimeout(function() {
            $(".overbox").css({
               "overflow": "hidden"
            })
            $(".box").addClass("back");
         }, 200)
         $(this).addClass('active').animate({
            "width": "700px",
            "height": "700px"
         });

         setTimeout(function() {
            $(".shape").css({
               "width": "50%",
               "height": "50%",
               "transform": "rotate(45deg)"
            });

            $(".overbox .title").fadeIn(300);
            $(".overbox .input").fadeIn(300);
            $(".overbox .button").fadeIn(300);
         }, 700)

         $(this).removeClass('material-button');

      }

      if ($(".alt-2").hasClass('material-buton')) {
         $(".alt-2").removeClass('material-buton');
         $(".alt-2").addClass('material-button');
      }

   });

});

$('ul li').each(function(){
	$(this).click(function(){
		if($(this).attr('rel') != "delete"){
			var num = $(this).attr('rel');
                        var temp = $(".input-number").val() + num;
                        $(".input-number").val(temp);
		}else{
			var temp = $(".input-number").val();
                        temp = temp.slice(0,temp.length-1);
                        $(".input-number").val(temp);
		}
	});
});

function getCustomerId()
{   
    $(".add-customer-icon-clear").html('');
    var customer_name=['8 Belly','Jessica Butler','Alen Cloony','Brandy Husel','Jacks Parow',
                        'Lan Josaph','Remestiriyo','Dawyn Johnson ','Peter Adam','Quanmtum Alex'];
                    
    
    
    customer_name.sort();
    //var str = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    for(var i=0;i<customer_name.length;i++)
    {
        $("#customerList").append('<li><div class="friends-grids">'+
                                            '<div class="grids-left">'+
                                                '<img src="img/co1.jpg">'+
                                            '</div>'+
                                            '<div class="grids-right">'+
                                                    '<h2 class="'+ customer_name[i]+' item" style="line-height: 34px;">'+ customer_name[i]+'</h2>'+
                                            '</div>'+
                                            '<div class="clear"> </div>'+
                                        '</div></li>');

    }
    $("#addCustomer").on('click',function () {
           $("#customerListForm").html(''); 
           $(this).css("background-color","#3e50b4"); 
           $(".add-customer-icon-add").html('');
           $(".add-customer-icon-clear").html('clear');
           $("#customerListForm").css('background-color','#3e50b4'); 
           $("#customerListForm").html('<div class="customer-main">'+
                                            '<form class="cbp-mc-form">'+
                                                    '<div class="cbp-mc-column">'+
                                                            '<label for="name">Name</label>'+
                                                            '<input type="text" id="name" name="name" placeholder="Enter Full Name">'+
                                                            '<label for="Department">Department</label>'+
                                                            '<input type="text" id="Department" name="Department" placeholder="Enter Department">'+
                                                            '<label for="Feild1">Feild 1</label>'+
                                                            '<input type="text" id="Feild1" name="Feild1" placeholder="Enter Feild1">'+
                                                            '<label for="Feild2">Feild2</label>'+
                                                            '<input type="text" id="Feild2" name="Feild2" placeholder="Enter Feild2" disabled>'+
                                                    '</div>'+
                                                    '<div class="cbp-mc-submit-wrap"><input class="cbp-mc-submit" type="button" value="Submit" /></div>'+
                                            '</form>'+
                                        '</div>'); 
    });    
}
function editBox()
{
    $("#cstmr-header").html('<form class="cbp-mc-form">'+
        '<div class="cbp-mc-column">'+
            '<textarea id="cstm-header-text" name="cstm-header-text"></textarea>'+
        '</div>'+
    '</form>');    
    $("#brandForm").html('<form class="cbp-mc-form">'+
        '<div class="cbp-mc-column">'+
            '<textarea id="brand-text" name="brand-text" style="border: 3px solid #000; color:#000 !important"></textarea>'+
        '</div>'+
    '</form>');
    $("#dprt-text").html('<form class="cbp-mc-form">'+
        '<div class="cbp-mc-column">'+
            '<input type="text" id="sell-pri-text" name="sell-pri-text" style="border: 3px solid #000; color:#000 !important">'+
        '</div>'+
    '</form>');
    $("#slg-pri").html('<form class="cbp-mc-form">'+
        '<div class="cbp-mc-column">'+
            '<input type="text" id="slg-text" name="slg-text" style="border: 3px solid #000; color:#000 !important">'+
        '</div>'+
    '</form>');
    $("#taxrate-text").html('<form class="cbp-mc-form">'+
        '<div class="cbp-mc-column">'+
            '<input type="text" id="tax-text" name="tax-text" style="border: 3px solid #000; color:#000 !important">'+
        '</div>'+
    '</form>');
    $("#weight-text").html('<form class="cbp-mc-form">'+
        '<div class="cbp-mc-column">'+
            '<input type="text" id="wgt-text" name="wgt-text" style="border: 3px solid #000; color:#000 !important">'+
        '</div>'+
    '</form>');
    $("#dimen-text").html('<form class="cbp-mc-form">'+
        '<div class="cbp-mc-column">'+
            '<input type="text" id="dim-text" name="dim-text" style="border: 3px solid #000; color:#000 !important">'+
        '</div>'+
    '</form>');
}
function getInventoryId()
{
    var inventory_name=['Hp laptop s2150','Keypad 2.0','Type writer','Coffee cup',
                        'Samsung grand','Printer dot matrix','Arizana Diet green Tea','Samsung Mouse',
                        'Dell Keybord'];
                    
    
    
        inventory_name.sort();
        //var str = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
        for(var i=0;i<inventory_name.length;i++)
        {
            $("#inventoryList").append('<div class="friends-grids">'+
                                                '<div class="grids-left">'+
                                                    '<img src="img/username.png">'+
                                                '</div>'+
                                                '<div class="grids-right">'+
                                                        '<h2 class="'+ inventory_name[i]+' item" >'+ inventory_name[i]+'</h2>'+
                                                        '<p>4 remaining in stock</p>'+
                                                '</div>'+
                                                '<div class="clear"> </div>'+
                                            '</div>');

        }
        
            
       

}


function getOrdersId()
{
    var order_name=['#12345 : Mr. Rameshwarma','#5235 : Mr. Kumar','Type Writer','Coffee Cup','Samsung Grand',
                        'Print Dot Matrix'];
                    
    
    
        order_name.sort();
        //var str = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
        for(var i=0;i<order_name.length;i++)
        {
            $("#orderList").append('<div class="friends-grids" style="border-bottom: 1px solid rgba(0,0,0,.12);">'+
                                                '<div class="grids-left">'+
                                                    '<img src="img/username.png">'+
                                                '</div>'+
                                                '<div class="grids-right">'+
                                                        '<h2 class="'+ order_name[i]+' item" ><b>'+ order_name[i]+'</b></h2>'+
                                                        '<p>4 remaining in stock</p>'+
                                                '</div>'+
                                                '<div class="clear"> </div>'+
                                            '</div>');
        }
}

function getAskId()
{
    var ask_name=['May i help you?','Item Complaints','Discussed Question',
                   'Delivery Orders Help','What do you want?','Payment problems',
                   'Service Question'];
                    
    
    
        ask_name.sort();
        for(var i=0;i<ask_name.length;i++)
        {
            $("#askList").append('<div class="mdl-card__ask-text meta mdl-color-text--grey-600">'+
                                    '<div class="ask-minilogo"></div>'+
                                    '<div>'+
                                      '<strong>'+ask_name[i]+'</strong>'+
                                      '<span>Review your account status</span>'+
                                    '</div>'+
                                  '</div>');
        }
}

function generateStars(numPadValLth) {
    var stars = '';
    for (var i=0; i< numPadValLth;i++){
        stars += '*';
    }
    return stars;
}
$(".numpad li").on('click',function() {
    if($(".numpad-input-number").val().length<4){
        var numpad_val=$(".numpad-input-number").val()+$(this).attr('rel');
        $(".numpad-input-number").val(generateStars(numpad_val.length));
    }
});

