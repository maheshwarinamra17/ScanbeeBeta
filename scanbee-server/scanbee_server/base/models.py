from django.db import models
from django.contrib.auth.models import User


ORG_TYPES = [(1, 'ONE'), (2, 'TWO'), (3, 'THREE')]
STORE_PRODUCT_STATUS = [(1, 'Continues'), (2, 'Discontinued')]
ORDER_STATUS_TYPES = [(1, 'Unsuccessful'), (2, 'Cancelled'), (3, 'Paid'), (4, 'Credit')]
NOTIFICATION_TYPES = [(1, 'Alert'), (2, 'Danger'), (3, 'Success')]
MODE_OF_PAYMENT_CHOICES = [(1, 'Cash'), (2, 'Card')]

class Organisation(models.Model):
	org_type = models.IntegerField(
		blank = False,
		choices = ORG_TYPES,
	) #incomplete description, choices required, or IntegerField
	org_name = models.CharField(
		blank = False,
		max_length = 255,
	)
	org_admin = models.ForeignKey(User)
	org_no_of_stores = models.IntegerField(
		blank = False,
	)

	def __str__(self):
		return self.org_name

class Store(models.Model):
	store_org = models.ForeignKey('Organisation')
	store_name = models.CharField(
		max_length = 255,
		blank = False,
	)
	store_address = models.CharField(
		max_length = 1023,
		blank = False,
	)
	store_latitude = models.DecimalField(
		max_digits=10, 
		decimal_places=6,
	)
	store_longitude = models.DecimalField(
		max_digits=10,
		decimal_places=6,
	)
	store_devices = models.CommaSeparatedIntegerField(
		max_length = 4,
	)
	store_phone = models.CharField(
		max_length = 12,
		unique = True,
	)
	store_work_start = models.TimeField()
	store_work_end = models.TimeField()

	def __str__(self):
		return self.store_org.org_name + "_" + self.store_name

class StoreProduct(models.Model):
	store_product_org = models.ForeignKey('Organisation')
	store_product_store = models.ForeignKey('Store')
	store_product_name = models.CharField(
		blank = False,
		max_length = 50,
		default = '',
	)
	store_product_timestamp = models.DateTimeField(
		auto_now = True,
	)
	store_product_description = models.TextField()
	store_product_selling_price = models.DecimalField(
		max_digits = 15,
		decimal_places = 2,
	)
	store_product_code = models.BigIntegerField(
		blank = False,
	)
	store_product_weight = models.DecimalField(
		max_digits = 15,
		decimal_places = 2,
	) #in grams
	store_product_quantity = models.IntegerField(
		default = 0,
	)
	store_product_status = models.IntegerField(
		choices = STORE_PRODUCT_STATUS,
		default = 1,
	)

	def __str__(self):
		return self.store_product_store.store_name + "_" + self.store_product_name

class Category(models.Model):
	category_name = models.CharField(
		max_length = 255,
	)

	def __str__(self):
		return self.category_name

class Group(models.Model):
	group_name = models.CharField(
		max_length = 255,
	)
	group_category = models.ForeignKey(Category)

	def __str__(self):
		return self.group_category.category_name + "_" + self.group_name

class ProductGroup(models.Model):
	product_group_product = models.ForeignKey(StoreProduct)
	product_group_group = models.ForeignKey(Group)
	product_group_category = models.ForeignKey(Category)

	class Meta:
		unique_together = ("product_group_product", "product_group_category")

	def __str__(self):
		return self.product_group_product.__str__() + "_" + self.product_group_group.__str__()

class Tax(models.Model):
	tax_name = models.CharField(
		max_length = 50,
	)
	tax_percentage = models.DecimalField(
		max_digits = 4,
		decimal_places = 2,
	)

	def __str__(self):
		return self.tax_name

class TaxProduct(models.Model):
	tax_product_tax = models.ForeignKey(Tax)
	tax_product_product = models.ForeignKey(StoreProduct)

class Order(models.Model):
	order_org = models.ForeignKey('Organisation')
	order_store = models.ForeignKey('Store')
	order_customer = models.ForeignKey('Customer')
	order_discount = models.FloatField(
		blank = False,
		default = 0.00,
	)
	order_amount = models.DecimalField(
		max_digits = 15,
		decimal_places = 2,
	)
	order_time = models.DateTimeField(
		auto_now = True,
	)
	order_notes = models.TextField(
		blank = True,
	)
	order_mode_of_payment = models.IntegerField(
		choices = MODE_OF_PAYMENT_CHOICES,
	)
	order_status = models.IntegerField(
		choices = ORDER_STATUS_TYPES,
	)

	def __str__(self):
		return self.order_customer.customer_name + "_" + self.order_time.strftime("%H-%M-%S-%d-%m-%Y")

class OrderLog(models.Model):
	order_log_order = models.ForeignKey('Order')
	order_log_store_product = models.ForeignKey('StoreProduct')
	order_log_quantity = models.IntegerField(
		blank = False,
		default = 0,
	)

class Notification(models.Model):
	notification_org = models.ForeignKey('Organisation')
	notification_store = models.ForeignKey('Store')
	notification_text = models.TextField(
		blank = False, 
		default = '',
	)
	notification_time = models.DateTimeField(
		auto_now = True,
	)
	notification_type = models.IntegerField(
		choices = NOTIFICATION_TYPES,
	)

	def __str__(self):
		return self.notification_time.strftime("%H-%M-%S-%d-%m-%Y")
	#need to complete the notification with more information

class StoreHour(models.Model):
	store_hour_org = models.ForeignKey('Organisation')
	store_hour_store = models.ForeignKey('Store')
	store_hour_start = models.TimeField()
	store_hour_end = models.TimeField()
	store_hour_user_checkin = models.ForeignKey(User, related_name = 'CheckInUser')
	store_hour_user_checkout = models.ForeignKey(User, related_name = 'CheckOutUser')

class Customer(models.Model):
	customer_name = models.CharField(
		max_length = 255,
		blank = False,
	)
	customer_phone = models.CharField(
		max_length = 10,
	)
	customer_address = models.TextField()
	customer_email = models.EmailField()

	def __str__(self):
		return self.customer_name + "_" + self.customer_phone
	#need to complete customer information table

#create a store credit table
class StoreCredit(models.Model):
	store_credit_store = models.ForeignKey(Store)
	store_credit_customer = models.ForeignKey(Customer)
	store_credit_amount = models.DecimalField(
		max_digits = 15,
		decimal_places = 2,
	)

class Ledger(models.Model):
	ledger_org = models.ForeignKey('Organisation')
	ledger_store = models.ForeignKey('Store')
	ledger_money_in = models.DecimalField(
		max_digits = 15,
		decimal_places = 2,
	)
	ledger_money_out = models.DecimalField(
		max_digits = 15,
		decimal_places = 2,
	)
	ledger_notes = models.TextField()
	ledger_timestamp = models.DateTimeField(
		auto_now = True,
	)
	ledger_user = models.ForeignKey(User)

