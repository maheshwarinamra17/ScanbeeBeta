# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='productgroup',
            name='product_group_category',
            field=models.ForeignKey(default=1, to='base.Category'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='ledger',
            name='ledger_timestamp',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='notification',
            name='notification_time',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='order_notes',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='order_time',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='storeproduct',
            name='store_product_status',
            field=models.IntegerField(default=1, choices=[(1, b'Continues'), (2, b'Discontinued')]),
        ),
        migrations.AlterField(
            model_name='storeproduct',
            name='store_product_timestamp',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterUniqueTogether(
            name='productgroup',
            unique_together=set([('product_group_product', 'product_group_category')]),
        ),
    ]
