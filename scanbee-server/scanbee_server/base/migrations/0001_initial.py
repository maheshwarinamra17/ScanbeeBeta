# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('customer_name', models.CharField(max_length=255)),
                ('customer_phone', models.CharField(max_length=10)),
                ('customer_address', models.TextField()),
                ('customer_email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('group_name', models.CharField(max_length=255)),
                ('group_category', models.ForeignKey(to='base.Category')),
            ],
        ),
        migrations.CreateModel(
            name='Ledger',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ledger_money_in', models.DecimalField(max_digits=15, decimal_places=2)),
                ('ledger_money_out', models.DecimalField(max_digits=15, decimal_places=2)),
                ('ledger_notes', models.TextField()),
                ('ledger_timestamp', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('notification_text', models.TextField(default=b'')),
                ('notification_time', models.DateTimeField()),
                ('notification_type', models.IntegerField(choices=[(1, b'Alert'), (2, b'Danger'), (3, b'Success')])),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_discount', models.FloatField(default=0.0)),
                ('order_amount', models.DecimalField(max_digits=15, decimal_places=2)),
                ('order_time', models.DateTimeField()),
                ('order_notes', models.TextField()),
                ('order_mode_of_payment', models.IntegerField(choices=[(1, b'Cash'), (2, b'Card')])),
                ('order_status', models.IntegerField(choices=[(1, b'Unsuccessful'), (2, b'Cancelled'), (3, b'Paid'), (4, b'Credit')])),
                ('order_customer', models.ForeignKey(to='base.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='OrderLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_log_quantity', models.IntegerField(default=0)),
                ('order_log_order', models.ForeignKey(to='base.Order')),
            ],
        ),
        migrations.CreateModel(
            name='Organisation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('org_type', models.IntegerField(choices=[(1, b'ONE'), (2, b'TWO'), (3, b'THREE')])),
                ('org_name', models.CharField(max_length=255)),
                ('org_no_of_stores', models.IntegerField()),
                ('org_admin', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ProductGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_group_group', models.ForeignKey(to='base.Group')),
            ],
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('store_name', models.CharField(max_length=255)),
                ('store_address', models.CharField(max_length=1023)),
                ('store_latitude', models.DecimalField(max_digits=10, decimal_places=6)),
                ('store_longitude', models.DecimalField(max_digits=10, decimal_places=6)),
                ('store_devices', models.CommaSeparatedIntegerField(max_length=4)),
                ('store_phone', models.CharField(unique=True, max_length=12)),
                ('store_work_start', models.TimeField()),
                ('store_work_end', models.TimeField()),
                ('store_org', models.ForeignKey(to='base.Organisation')),
            ],
        ),
        migrations.CreateModel(
            name='StoreCredit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('store_credit_amount', models.DecimalField(max_digits=15, decimal_places=2)),
                ('store_credit_customer', models.ForeignKey(to='base.Customer')),
                ('store_credit_store', models.ForeignKey(to='base.Store')),
            ],
        ),
        migrations.CreateModel(
            name='StoreHour',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('store_hour_start', models.TimeField()),
                ('store_hour_end', models.TimeField()),
                ('store_hour_org', models.ForeignKey(to='base.Organisation')),
                ('store_hour_store', models.ForeignKey(to='base.Store')),
                ('store_hour_user_checkin', models.ForeignKey(related_name='CheckInUser', to=settings.AUTH_USER_MODEL)),
                ('store_hour_user_checkout', models.ForeignKey(related_name='CheckOutUser', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='StoreProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('store_product_name', models.CharField(default=b'', max_length=50)),
                ('store_product_timestamp', models.DateTimeField()),
                ('store_product_description', models.TextField()),
                ('store_product_selling_price', models.DecimalField(max_digits=15, decimal_places=2)),
                ('store_product_code', models.BigIntegerField()),
                ('store_product_weight', models.DecimalField(max_digits=15, decimal_places=2)),
                ('store_product_quantity', models.IntegerField(default=0)),
                ('store_product_status', models.IntegerField(choices=[(1, b'Continues'), (2, b'Discontinued')])),
                ('store_product_org', models.ForeignKey(to='base.Organisation')),
                ('store_product_store', models.ForeignKey(to='base.Store')),
            ],
        ),
        migrations.CreateModel(
            name='Tax',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tax_name', models.CharField(max_length=50)),
                ('tax_percentage', models.DecimalField(max_digits=4, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='TaxProduct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tax_product_product', models.ForeignKey(to='base.StoreProduct')),
                ('tax_product_tax', models.ForeignKey(to='base.Tax')),
            ],
        ),
        migrations.AddField(
            model_name='productgroup',
            name='product_group_product',
            field=models.ForeignKey(to='base.StoreProduct'),
        ),
        migrations.AddField(
            model_name='orderlog',
            name='order_log_store_product',
            field=models.ForeignKey(to='base.StoreProduct'),
        ),
        migrations.AddField(
            model_name='order',
            name='order_org',
            field=models.ForeignKey(to='base.Organisation'),
        ),
        migrations.AddField(
            model_name='order',
            name='order_store',
            field=models.ForeignKey(to='base.Store'),
        ),
        migrations.AddField(
            model_name='notification',
            name='notification_org',
            field=models.ForeignKey(to='base.Organisation'),
        ),
        migrations.AddField(
            model_name='notification',
            name='notification_store',
            field=models.ForeignKey(to='base.Store'),
        ),
        migrations.AddField(
            model_name='ledger',
            name='ledger_org',
            field=models.ForeignKey(to='base.Organisation'),
        ),
        migrations.AddField(
            model_name='ledger',
            name='ledger_store',
            field=models.ForeignKey(to='base.Store'),
        ),
        migrations.AddField(
            model_name='ledger',
            name='ledger_user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
