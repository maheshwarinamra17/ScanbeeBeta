from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'organisation', views.OrganisationViewSet)
router.register(r'stores', views.StoreViewSet)
router.register(r'storeproducts', views.StoreProductViewSet)
router.register(r'categories', views.CategoryViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'taxes', views.TaxViewSet)
router.register(r'orders', views.OrderViewSet)
router.register(r'orderlogs', views.OrderLogViewSet)
router.register(r'notifications', views.NotificationViewSet)
router.register(r'storehours', views.StoreHourViewSet)
router.register(r'customers', views.CustomerViewSet)
router.register(r'storecredits', views.StoreCreditViewSet)
router.register(r'ledgers', views.LedgerViewSet)
router.register(r'users', views.UserViewSet)