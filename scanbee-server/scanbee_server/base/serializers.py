from rest_framework import serializers
from .models import *
from rest_framework.exceptions import APIException
from django.core.exceptions import ValidationError
from django.db import Error
from rest_framework.reverse import reverse
from django.db.models.signals import post_delete
from django.dispatch import receiver

tax_ids = list(Tax.objects.values_list('id', 'tax_name'))
group_ids = list(Group.objects.values_list('id', 'group_name'))

@receiver(post_delete, sender=Tax)
def delete_from_tax_list(sender, instance, **kwargs):
    tax_ids.remove((instance.id, instance.tax_name))

@receiver(post_delete, sender=Group)
def delete_from_group_list(sender, instance, **kwargs):
    group_ids.remove((instance.id, instance.group_name))

class GroupCategoryError(APIException):
	status_code = 400
	default_detail = 'Product group with this Product group product and Product group category already exists.'


class OrganisationSerializer(serializers.ModelSerializer):

	org_type_display = serializers.SerializerMethodField()
	class Meta:
		model = Organisation
		fields = ('org_name', 'org_type', 'org_type_display', 'org_admin', 'org_no_of_stores', )

	def get_org_type_display(self, obj):
		return obj.get_org_type_display()

class StoreSerializer(serializers.ModelSerializer):

	class Meta:
		model = Store
		fields = ('id', 'store_org', 'store_name', 'store_address', 'store_latitude', 'store_longitude', 
				  'store_devices', 'store_phone', 'store_work_start', 'store_work_end', )

class CategorySerializer(serializers.ModelSerializer):

	class Meta:
		model = Category
		fields = ('id', 'category_name', )

class GroupSerializer(serializers.ModelSerializer):

	class Meta:
		model = Group
		fields = ('id', 'group_name', 'group_category', )

	def create(self, validated_data):
		instance = self.Meta.model(**validated_data)
		instance.save()
		group_ids.append(tuple([instance.id, instance.group_name]))
		return instance

class TaxSerializer(serializers.ModelSerializer):

	class Meta:
		model = Tax
		fields = ('id', 'tax_name', 'tax_percentage', )

	def create(self, validated_data):
		instance = self.Meta.model(**validated_data)
		instance.save()
		tax_ids.append(tuple([instance.id, instance.tax_name]))
		return instance

class StoreProductSerializer(serializers.ModelSerializer):
	store_product_status_display = serializers.SerializerMethodField()
	store_product_group_display = serializers.SerializerMethodField()
	store_product_tax_display = serializers.SerializerMethodField()
	store_product_tax = serializers.MultipleChoiceField(
		choices = tax_ids,
		write_only = True,
	)
	store_product_group = serializers.MultipleChoiceField(
		choices = group_ids,
		write_only = True,
	)

	class Meta:
		model = StoreProduct
		fields = ('id', 'store_product_org', 'store_product_store', 'store_product_name', 'store_product_timestamp',
				  'store_product_description', 'store_product_selling_price', 'store_product_code',
				  'store_product_weight', 'store_product_quantity', 'store_product_status', 'store_product_status_display', 
				  'store_product_group_display', 'store_product_tax_display', 'store_product_tax', 'store_product_group', )

	def create(self, validated_data):
		tax_values = validated_data.pop('store_product_tax')
		group_values = validated_data.pop('store_product_group')
		product_instance = self.Meta.model(**validated_data)
		product_instance.save()
		for i in tax_values:
			tax_instance = TaxProduct(tax_product_tax = Tax.objects.filter(id = i)[0], tax_product_product = product_instance)
			tax_instance.save()
		for i in group_values:
			group_instance = Group.objects.filter(id = i)[0]
			product_group_instance = ProductGroup(product_group_group = group_instance, product_group_product = product_instance, product_group_category = group_instance.group_category)
			try:
				product_group_instance.full_clean(validate_unique=True)
				product_group_instance.save()
			except ValidationError as e:
				raise(GroupCategoryError)
		return product_instance

	def update(self, instance, validated_data):
		tax_values = validated_data.pop('store_product_tax')
		group_values = validated_data.pop('store_product_group')
		for attr, value in validated_data.items():
			setattr(instance, attr, value)
		instance.save()
		tax_present = TaxProduct.objects.filter(tax_product_product__id = instance.id)
		tax_present_ids = list(tax_present.values_list('id', flat=True))
		group_present = ProductGroup.objects.filter(product_group_product__id = instance.id)
		group_present_ids = list(group_present.values_list('id', flat=True))
		for i in tax_values:
			tax_product_item = tax_present.filter(tax_product_tax__id = i)
			if len(tax_product_item):
				tax_present_ids.remove(tax_product_item[0].id)
			else:
				tax_instance = TaxProduct(tax_product_tax = Tax.objects.filter(id = i)[0], tax_product_product = instance)
				tax_instance.save()
		for i in tax_present_ids:
			tax_present.filter(id = i).delete()
		for i in group_values:
			product_group_item = group_present.filter(product_group_group__id = i)
			if len(product_group_item):
				group_present_ids.remove(product_group_item[0].id)
			else:
				group_instance = Group.objects.filter(id = i)[0]
				product_group_instance = ProductGroup(product_group_group = group_instance, product_group_product = instance, product_group_category = group_instance.group_category)
				try:
					product_group_instance.full_clean(validate_unique=True)
					product_group_instance.save()
				except ValidationError as e:
					raise(GroupCategoryError)
		for i in group_present_ids:
			group_present.filter(id = i).delete()
		return instance

	def get_store_product_status_display(self, obj):
		return obj.get_store_product_status_display()

	def get_store_product_group_display(self, obj):
		group_response = {}
		query_set = ProductGroup.objects.filter(product_group_product = obj.pk)
		for query in query_set:
			group_response[query.product_group_group.group_category.category_name] = query.product_group_group.group_name
		return group_response

	def get_store_product_tax_display(self, obj):
		tax_response = {}
		query_set = TaxProduct.objects.filter(tax_product_product = obj.pk)
		for query in query_set:
			tax_response[query.tax_product_tax.tax_name] = query.tax_product_tax.tax_percentage
		return tax_response

class OrderSerializer(serializers.ModelSerializer):

	order_mode_of_payment_display = serializers.SerializerMethodField()
	order_status_display = serializers.SerializerMethodField()
	order_order_log_display = serializers.SerializerMethodField()
	order_order_log = serializers.DictField(
		child = serializers.IntegerField(min_value = 1),
		write_only = True,
	)

	class Meta:
		model = Order
		fields = ('id', 'order_org', 'order_store', 'order_customer', 'order_discount', 'order_amount', 
				  'order_time', 'order_notes', 'order_mode_of_payment', 'order_status', 
				  'order_mode_of_payment_display', 'order_status_display', 'order_order_log_display', 
				  'order_order_log', )

	def create(self, validated_data):
		order_log_values = validated_data.pop('order_order_log')
		order_instance = self.Meta.model(**validated_data)
		order_instance.save()
		for i in order_log_values.keys():
			product_instance = StoreProduct.objects.filter(id = int(i))[0]
			quantity = order_log_values[i]
			order_log_instance = OrderLog(order_log_order = order_instance, order_log_store_product = product_instance, order_log_quantity = quantity)
			order_log_instance.save()
		return order_instance

	def update(self, instance, validated_data):
		order_log_values = validated_data.pop('order_order_log')
		for attr, value in validated_data.items():
			setattr(instance, attr, value)
		instance.save()
		order_log_present = OrderLog.objects.filter(order_log_order__id = instance.id)
		order_log_present_ids = list(order_log_present.values_list('id', flat=True))
		for i in order_log_values.keys():
			order_log_item = order_log_present.filter(order_log_store_product__id = int(i)).filter(order_log_quantity = order_log_values[i])
			if len(order_log_item):
				order_log_present_ids.remove(order_log_item[0].id)
			else:
				product_instance = StoreProduct.objects.filter(id = int(i))[0]
				quantity = order_log_values[i]
				order_log_instance = OrderLog(order_log_order = instance, order_log_store_product = product_instance, order_log_quantity = quantity)
				order_log_instance.save()
		for i in order_log_present_ids:
			order_log_present.filter(id = i).delete()
		return instance

	def get_order_mode_of_payment_display(self, obj):
		return obj.get_order_mode_of_payment_display()

	def get_order_status_display(self, obj):
		return obj.get_order_status_display()

	def get_order_order_log_display(self, obj):
		order_log_response = {}
		query_set = OrderLog.objects.filter(order_log_order = obj.pk)
		for query in query_set:
			order_log_response[query.order_log_store_product.store_product_name] = query.order_log_quantity
		return order_log_response

class OrderLogSerializer(serializers.ModelSerializer):

	class Meta:
		model = OrderLog
		fields = ('id', 'order_log_order', 'order_log_store_product', 'order_log_quantity', )

class NotificationSerializer(serializers.ModelSerializer):

	notification_type_display = serializers.SerializerMethodField()
	class Meta:
		model = Notification
		fields = ('id', 'notification_org', 'notification_store', 'notification_text', 'notification_time', 
				  'notification_type_display')

	def get_notification_type_display(self, obj):
		return obj.get_notification_type_display()

class StoreHourSerializer(serializers.ModelSerializer):

	class Meta:
		model = StoreHour
		fields = ('id', 'store_hour_org', 'store_hour_store', 'store_hour_start', 'store_hour_end', 
				  'store_hour_user_checkin', 'store_hour_user_checkout', )

class CustomerSerializer(serializers.ModelSerializer):

	class Meta:
		model = Customer
		fields = ('id', 'customer_name', 'customer_phone', 'customer_address', 'customer_email', )

class StoreCreditSerializer(serializers.ModelSerializer):

	class Meta:
		model = StoreCredit
		fields = ('id', 'store_credit_store', 'store_credit_customer', 'store_credit_amount', )

class LedgerSerializer(serializers.ModelSerializer):

	class Meta:
		model = Ledger
		fields = ('id', 'ledger_org', 'ledger_store', 'ledger_money_in', 'ledger_money_out', 'ledger_notes', 
				  'ledger_timestamp', 'ledger_user', )

class UserSerializer(serializers.ModelSerializer):

	full_name = serializers.CharField(source = 'get_full_name', read_only = True)
	links = serializers.SerializerMethodField()
	class Meta:
		model = User
		fields = ('id', User.USERNAME_FIELD, 'full_name', 'email', 'password', 'links',)
		extra_kwargs = {'password' : {'write_only' : True}}

	# def create(self, validated_data):
	# 	password = validated_data.pop('password', None)
	# 	instance = self.Meta.model(**validated_data)
	# 	if password is not None:
	# 		instance.set_password(password)
	# 	instance.save()
	# 	return instance

	# def update(self, instance, validated_data):
	# 	for attr, value in validated_data.items():
	# 		if attr == 'password':
	# 			instance.set_password(value)
	# 		else:
	# 			setattr(instance, attr, value)
	# 	instance.save()
	# 	return instance

	def get_links(self, obj):
		request = self.context['request']
		username = obj.get_username()
		return {
			'self': reverse('user-detail', kwargs = {User.USERNAME_FIELD: username}, request = request),
		}