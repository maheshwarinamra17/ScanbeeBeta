from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Tb_user(models.Model):
	user_name = models.CharField(
		max_length = 255,
		blank = False,
		default = '',
	)
	user_email = models.EmailField(
		max_length = 255,
		unique = True,
	)
	user_phone = models.CharField(
		max_length = 12,
		unique = True,
	)
	user_city = models.CharField(
		max_length = 20,
	)
	user_password = models.CharField(
		unique = True,
		blank = False,
	)
	user_pin = models.CharField(
		max_length = 4,
		unique = True,
		blank = False,
	)
	user_permissions = models.IntegerField(
		default = 3,
		choices = [0,1,2,3],
		blank = False,
	)
	user_org = models.ForeignKey('Tb_organisation')

class Tb_user_log(models.Model):
	log_user = models.ForeignKey('Tb_user')
	log_timestamp_start = models.DateTimeField(
		#check for flags here
	)
	log_timestamp_end = models.DateTimeField(
	)
	log_store = models.ForeignKey('Tb_store')
	log_org = models.ForeignKey('Tb_organisation')
	log_session_flag = models.BooleanField(
		default = True
	)	#True for old session, False for new session

class Tb_organisation(models.Model):
	org_type = models.CharField(
	) #incomplete description, choices required, or IntegerField
	org_name = models.CharField(
		blank = False,
		max_length = 255,
	)
	org_admin = models.ForeignKey('Tb_user')
	org_no_of_stores = models.IntegerField(
		blank = False,
	)
	org_db_name = models.CharField(
	) #need to discuss

class Tb_product(models.Model):
	product_name = models.CharField(
		max_length = 255,
		blank = False
	)

	#complete description required

class Tb_device(models.Model):
	device_name = models.CharField(
	)

	#again ambiguous

class Tb_category(models.Model):
	category_name = models.Model(
		max_length = 30,
		blank = False,
	)
	category_desc = models.TextField()

class Tb_group(models.Model):
	group_category = models.ForeignKey('Tb_category')
	group_name = models.Model(
		max_length = 30,
		blank = False,
	)
	group_desc = models.TextField()

#On study it turns out that Django supports multiple databases, but they cannot with each other, but within each other only
#Plus, we need to create databases, on the fly, which is the work of a script and Django does not specifically provide for it.
#A basic description for a single organisation can be found below.

class Tb_store(models.Model):
	store_org = models.ForeignKey('Tb_organisation')
	store_name = models.CharField(
		max_length = 255,
		blank = False,
	)
	store_address = models.CharField(
		max_length = 1023,
		blank = False,
	)
	store_latitude = models.DecimalField(
		max_digits=10, 
		decimal_places=6,
	)
	store_longitude = models.DecimalField(
		max_digits=10,
		decimal_places=6,
	)
	store_devices = models.CommaSeparatedIntegerField(
		max_length = 4,
	)
	store_phone = models.CharField(
		max_length = 12,
		unique = True,
	)
	store_work_start = models.TimeField()
	store_work_end = models.TimeField()

class Tb_store_product(models.Model):
	store_product_org = models.ForeignKey('Tb_organisation')
	store_product_store = models.ForeignKey('Tb_store')
	store_product_product = models.ForeignKey('Tb_product')

class Tb_order(models.Model):
	order_org = models.ForeignKey('Tb_organisation')
	order_store = models.ForeignKey('Tb_store')
	order_customer = models.ForeignKey('Tb_customer')

class Tb_notification(models.Model):
	notification_org = models.ForeignKey('Tb_organisation')
	notification_store = models.ForeignKey('Tb_store')

class Tb_store_hour(models.Model):
	store_hour_org = models.ForeignKey('Tb_organisation')
	store_hour_store = models.ForeignKey('Tb_store')
	store_hour_start = models.TimeField()
	store_hour_end = models.TimeField()
	store_hour_user_checkin = models.ForeignKey('Tb_user')
	store_hour_user_checkout = models.ForeignKey('Tb_user')

class Tb_customer(models.Model):
	customer_name = models.CharField(
		max_length = 255,
		blank = False,
	)

class Tb_ledger(models.Model):
	pass

class Tb_setting(models.Model):
	pass
