from django.shortcuts import render
from rest_framework import viewsets, authentication, permissions, filters
from .models import *
from .serializers import *
# Create your views here.


class DefaultsMixin(object):

	# authentication_classes = (
	# 	authentication.BasicAuthentication,
	# 	authentication.TokenAuthentication,
	# )

	# permission_classes = (
	# 	permissions.IsAuthenticated,
	# )

	paginate_by = 25
	paginate_by_param = 'page_size'
	max_paginate_by = 100

	filter_backends = (
		filters.DjangoFilterBackend,
		filters.SearchFilter,
		filters.OrderingFilter,
	)

class OrganisationViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Organisation involved"""

	queryset = Organisation.objects.all()
	serializer_class = OrganisationSerializer
	search_fields = ('org_name', )
	ordering_fields = ('org_name', )

class StoreViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Stores opened by the organisation"""

	queryset = Store.objects.all()
	serializer_class = StoreSerializer
	search_fields = ('store_org', 'store_name', 'store_phone', 'store_latitude', 'store_longitude', )
	ordering_fields = ('store_org', 'store_name', )

class StoreProductViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""What store sells what product!"""

	queryset = StoreProduct.objects.all()
	serializer_class = StoreProductSerializer
	search_fields = ('store_product_org', 'store_product_store', 'store_product_product', )
	ordering_fields = ('store_product_org', 'store_product_store', 'store_product_product', )

class CategoryViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Categories for tagging"""

	queryset = Category.objects.all()
	serializer_class = CategorySerializer
	search_fields = ('category_name', )
	ordering_fields = ('category_name', )

class GroupViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Groups for tagging"""

	queryset = Group.objects.all()
	serializer_class = GroupSerializer
	search_fields = ('group_name', 'group_category', )
	ordering_fields = ('group_name', 'group_category', )

class TaxViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Taxes defined"""

	queryset = Tax.objects.all()
	serializer_class = TaxSerializer
	search_fields = ('tax_name', 'tax_percentage', )
	ordering_fields = ('tax_name', 'tax_percentage', )

class OrderViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Orders from customers to stores."""

	queryset = Order.objects.all()
	serializer_class = OrderSerializer
	search_fields = ('order_org', 'order_store', 'order_customer', )
	ordering_fields = ('order_org', 'order_store', 'order_customer', 'order_amount', 'order_time', )

class OrderLogViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Order to Products mapping"""

	queryset = OrderLog.objects.all()
	serializer_class = OrderLogSerializer
	search_fields = ('order_log_order', )
	ordering_fields = ('order_log_order', )

class NotificationViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Some notification system"""

	queryset = Notification.objects.all()
	serializer_class = NotificationSerializer
	search_fields = ('notification_org', 'notification_store', )
	ordering_fields = ('notification_org', 'notification_store', )

class StoreHourViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Details about store times"""

	queryset = StoreHour.objects.all()
	serializer_class = StoreHourSerializer
	search_fields = ('store_hour_org', 'store_hour_store', )
	ordering_fields = ('store_hour_org', 'store_hour_store', 'store_hour_start', 'store_hour_end', 
				  'store_hour_user_checkin', 'store_hour_user_checkout', )

class CustomerViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Customer Details"""

	queryset = Customer.objects.all()
	serializer_class = CustomerSerializer
	search_fields = ('customer_name', )
	ordering_fields = ('customer_name', )

class StoreCreditViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Credit details store to customer"""

	queryset = StoreCredit.objects.all()
	serializer_class = StoreCreditSerializer
	search_fields = ('store_credit_store', 'store_credit_customer', 'store_credit_amount', )

class LedgerViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""Ledger details"""

	queryset = Ledger.objects.all()
	serializer_class = LedgerSerializer
	
class UserViewSet(DefaultsMixin, viewsets.ModelViewSet):
	"""The User model that has not yet been defined."""

	lookup_field = User.USERNAME_FIELD
	lookup_field_kwarg = User.USERNAME_FIELD
	queryset = User.objects.order_by(User.USERNAME_FIELD)
	serializer_class = UserSerializer
	search_fields = (User.USERNAME_FIELD, )